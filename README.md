# Onboarding FIF #

En este repositorio se encuentra  documentación y presentaciones para iniciar el desarrollo con Nodejs


### Para qué sirve? ###

* Entregar información básica para setear ambiente de desarrollo
* Presentaciones sobre Nodejs

### Herramientas y tecnologías ###
* Linux
* Git
* NodeJs
* Npm
* Terminator (consola)
* Visual Studo Code
* Postman
* Soapui
* Docker y Docker-compose



### Instalaciones ###

* Se debe instalar Linux
* **Git** :  
https://www.digitalocean.com/community/tutorials/how-to-install-git-on-ubuntu-18-04
* **Nodejs**: 
https://tecadmin.net/install-latest-nodejs-npm-on-ubuntu/
https://websiteforstudents.com/install-the-latest-node-js-and-nmp-packages-on-ubuntu-16-04-18-04-lts/
* **Visual Studio Code**:
https://linuxize.com/post/how-to-install-visual-studio-code-on-ubuntu-18-04/
* **Docker**:
https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-16-04
https://cwiki.apache.org/confluence/pages/viewpage.action?pageId=94798094

* **Postman**: 
https://linuxize.com/post/how-to-install-postman-on-ubuntu-18-04/

* **SoapUi**: 
https://www.softwaretestinghelp.com/soapui-tutorial-4-soapui-download-and-install/

* **Terminator**: 
https://blog.arturofm.com/install-terminator-terminal-emulator-in-ubuntu/


### Lecturas básicas ###

* **Bash**:
https://www.howtogeek.com/67469/the-beginners-guide-to-shell-scripting-the-basics/
https://linuxconfig.org/bash-scripting-tutorial-for-beginners
* **Git**:
http://rogerdudler.github.io/git-guide/?ref=hackr.io
https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet
* **NodeJs y Npm**:
https://www.sitepoint.com/beginners-guide-node-package-manager/
https://blog.codeship.com/node-js-tutorial/
https://hackernoon.com/9-npm-packages-to-explore-in-your-next-node-js-application-2018-ec4aac0ca723

* **Docker**:
https://wsvincent.com/beginners-guide-to-docker/
https://medium.com/codingthesmartway-com-blog/docker-beginners-guide-part-1-images-containers-6f3507fffc98
https://www.linode.com/docs/applications/containers/how-to-use-docker-compose/
https://takacsmark.com/docker-compose-tutorial-beginners-by-example-basics/

### Prácticas online ###

* **Bash**:
https://www.tutorialspoint.com/unix_terminal_online.php

* **Git**:
https://learngitbranching.js.org/
https://www.katacoda.com/courses/git
* **NodeJs**:
https://codesandbox.io/s/node-js-sample-k4b26
* **Docker**:
https://www.katacoda.com/courses/docker
https://www.katacoda.com/courses/docker-orchestration
https://www.katacoda.com/courses/container-runtimes
https://www.katacoda.com/courses/dotnet-in-docker/deploying-nodejs-express-as-docker-container